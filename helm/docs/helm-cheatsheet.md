# Helm

# Helm Cheat Sheet

## Recommended readings

- ["Learning Helm" by Matt Farina, Matt Butcher and Josh Dolitsky](https://learning.oreilly.com/library/view/learning-helm/9781492083641/)

## Install [helm](https://helm.sh/)

## Install [helm-gcs](https://github.com/hayorov/helm-gcs)

helm-gcs is a helm plugin that allows to manage private helm repositories on Google Cloud Storage aka buckets.

## Install [helm-git](https://github.com/aslafy-z/helm-git)

helm-git is a helm plugin that provides GIT protocol support for private helm repositories.

## Cheat Sheet

[Helm Documentation - Commands](https://helm.sh/docs/helm/helm/)

| Description                                 | Command                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Example                                                                                                                                                                                                                                                                                                    |
| ------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Add repository                              | `helm repo add <repository-name> <repository-url>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | `helm repo add airflow-stable https://airflow-helm.github.io/charts`                                                                                                                                                                                                                                       |
| Update repository                           | `helm repo update` <br/><br/>_Note: This is required to refresh local index if new versions of charts have been released in the remote repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | `helm repo update`                                                                                                                                                                                                                                                                                         |
| List repositories                           | `helm repo list`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | `helm repo list`                                                                                                                                                                                                                                                                                           |
| Search repositories                         | `helm search repo <key-word>`<br/>&nbsp;&nbsp;&nbsp;`--versions`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | `helm search repo airflow --versions` <br/><br/> _Description: Returns all versions of charts which have a name or a description containing a word "airflow". By default the latest version of a chart if the "--versions" flag is not specified._                                                         |
| Install application                         | `helm install <release-name> <repository-name>/<chart-name>`<br/>&nbsp;&nbsp;&nbsp;`--version <version-number>`<br/>&nbsp;&nbsp;&nbsp;`--namespace <namespace-name>`<br/>&nbsp;&nbsp;&nbsp;`--wait --timeout <seconds>` _- instructs helm to wait for pods to start. In case of pods do not start before timeout then it marks installation as failed._<br/>&nbsp;&nbsp;&nbsp;`--generate-name [--template-name <template-rule>]` - _installation name is autogenerated a combination of the chart name and a timestamp; the "--template-name" flag alows to customize autogenerated name_<br/>&nbsp;&nbsp;&nbsp;`--create-namespace <namespace-name>`<br/>&nbsp;&nbsp;&nbsp;`--dry-run` - _produces yaml file with injected values without executing installation_                                                                                                                                                                                                                                              | `helm install airflow airflow-stable/airflow --values ./custom-values.yaml`<br/><br/>_Description: Install the latest version of the airflow char in the default namespace_ <br/><br/>`helm install airflow airflow-stable/airflow --version 1.10.10 --namespace default --values ./custom-values.yaml`    |
| Install application with custom values      | `helm install <release-name> <repository-name>/<chart-name>`<br />&nbsp;&nbsp;&nbsp;`--set <property-name>=<property-value>` <br/><br/>`helm install <release-name> <repository-name>/<chart-name>`<br/>&nbsp;&nbsp;&nbsp;`--values <file-name>.yaml` <br/><br/>_Note: You can specify the --values flag multiple times. This feature can be used to have "common" overrides in one file, and specific overrides in another._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | `helm install airflow airflow-stable/airflow`<br />&nbsp;&nbsp;&nbsp;`--set username=admin`<br/><br/>`helm install airflow airflow-stable/airflow`<br />&nbsp;&nbsp;&nbsp;`--values ./custom-values.yaml`                                                                                                  |
| Upgrade application                         | `helm upgrade <release-name> <repository-name>/<chart-name>`<br/>&nbsp;&nbsp;&nbsp;`--version <version-number>`<br/>&nbsp;&nbsp;&nbsp;--namespace \<namespace-name\><br/>&nbsp;&nbsp;&nbsp;`--wait --timeout <seconds>` _- instructs helm to wait for pods to start; in case of pods do not start before timeout then it marks installation as failed_<br/>&nbsp;&nbsp;&nbsp;`--atomic --timeout <seconds>` _- instructs helm to wait for pods to start; in case of pods do not start before timeout then it marks installation as failed and executes rollback operation to the last successful release<br/>&nbsp;&nbsp;&nbsp;`--cleanup-on-fail` _- requests deletion on every object that was newly created during the upgrade which failed._<br/>&nbsp;&nbsp;&nbsp;`--force` _- forces pods restarts by removing and recreating resources; it shouldn't be used as part of regular CI/CD pipeline.*<br/>&nbsp;&nbsp;&nbsp;`--dry-run` - *produces yaml file with injected values without executing upgrade\* | `helm upgrade airflow airflow-stable/airflow`                                                                                                                                                                                                                                                              |
| Upgrade application to update custom values | `helm upgrade <release-name> <repository-name>/<chart-name>`<br/>&nbsp;&nbsp;&nbsp;`--set <property-name>=<property-value>` _- overwrites a kay-value pair provided in a file defined by "--values" option._ <br/><br/>`helm upgrade <release-name> <repository-name>/<chart-name>`<br/>&nbsp;&nbsp;&nbsp;`--values <file-name>.yaml` <br/><br/>`helm upgrade <release-name> <repository-name>/<chart-name>` <br />&nbsp;&nbsp;&nbsp;`--reuse-values` _- re-uses configuration values used either during installation or the latest upgrade_<br/><br/> _Note: It is possible to upgrade the version of the chart and the configuration of the installation at the same time._                                                                                                                                                                                                                                                                                                                                    | `helm upgrade airflow airflow-stable/airflow` <br/>&nbsp;&nbsp;&nbsp;`--set username=admin` <br/><br/> helm upgrade airflow airflow-stable/airflow <br/>&nbsp;&nbsp;&nbsp;`--values ./custom-values.yaml`<br/><br/>`helm upgrade airflow airflow-stable/airflow` <br/>&nbsp;&nbsp;&nbsp;_`--reuse-values`_ |
| Upgrade or install application              | `helm upgrade --install <release-name> <repository-name>/<chart-name>` <br/><br/>_Note: This command checks if an application is installed or not and then either executes install or upgrade procedure._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `helm upgrade --install airflow airflow-stable/airflow`                                                                                                                                                                                                                                                    |
| Rollback application                        | `helm rollback <release-name>`<br/>&nbsp;&nbsp;&nbsp;`--revision <revision-number>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | `helm rollback airflow --revision 2`                                                                                                                                                                                                                                                                       |
| Uninstall application                       | `helm uninstall <release-name>` <br/>&nbsp;&nbsp;&nbsp;`--namespace <namespace-name>` <br/>&nbsp;&nbsp;&nbsp;`--keep-history` _- allows to keep records with details of releases and only if this option is used there is possibility to rollback application removal process._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | `helm uninstall airflow`                                                                                                                                                                                                                                                                                   |
| List installations                          | `helm list [--all-namespaces]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | `helm list --all-namespaces`                                                                                                                                                                                                                                                                               |
| Get configuration values of a release       | `helm get values <release-name>` <br/>&nbsp;&nbsp;&nbsp;`--revision <revision-number>`<br/>&nbsp;&nbsp;&nbsp;`--all` _- get all of the values currently set for a release. In other case only values moodified in a release are returned_                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `helm get values airflow --revision 2`                                                                                                                                                                                                                                                                     |
| Get release notes of a release              | `helm get notes <release-name>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | `helm get notes airflow`                                                                                                                                                                                                                                                                                   |
| Get yaml manifest for a release             | `helm get manifest <release-name>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | `helm get manifest airflow`                                                                                                                                                                                                                                                                                |
| Return default values defined in a chart    | `helm inspect values <repository-name>/<chart-name>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | `helm inspect values airflow-stable/airflow`                                                                                                                                                                                                                                                               |
| Get history of a release                    | `helm history <release-name>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | `helm history airflow`                                                                                                                                                                                                                                                                                     |
| Create a new chart                          | `helm create <chart-name>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | `helm create app-nifi`                                                                                                                                                                                                                                                                                     |
| Package a chart                             | `helm package <chart-name>`<br/>&nbsp;&nbsp;&nbsp;`--dependency-update (-u)`<br/>&nbsp;&nbsp;&nbsp;`--destination (-d)` _- sets the location to put the chart archive if it is different from the current working directory._<br/><br/>_Note: The .helmignore file provides a place to specify a list of files to skip. This file needs to be at the top level of the chart._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | `helm package app-nifi`                                                                                                                                                                                                                                                                                    |
| Chart validation                            | `helm lint <chart-name>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | `helm lint app-nifi`                                                                                                                                                                                                                                                                                       |
| Lists chart templates                       | `helm template <chart-name>`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | `helm template app-nifi`                                                                                                                                                                                                                                                                                   |
| Update a chart's dependencies               | helm dependency update .                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | helm dependency update .                                                                                                                                                                                                                                                                                   |

## Helm Installation States

Once an installation exceeds ten releases, Helm deletes the oldest release records until no more than the maximum remain.
During the life cycle of a release, it can pass through several different statuses:

- pending-install
- deployed
- pending-upgrade
- superseded - when an upgrade is run, the last deployed release is updated, marked as superseded, and the newly upgraded release is changed from pending-upgrade to deployed
- pending-rollback
- uninstalling
- uninstalled
- failed - if during any operation, Kubernetes rejects a manifest submitted by Helm, Helm will mark that release failed

## Helm Release Information

By default Helm stores information about different releases of an application as Kubernetes secrets sh.helm.release.v1.\<release-name\>.v\<version-number\> (though there are other supported storage backends).
This information can be access with this command

```javascript
kubectl get secrets
```

## Helm Variables

- .Values.<Name> - values.yaml etc ...
- .Charts.<Name> - i.e. Name, Version, AppVersion, Annotations
- .Releases.<Name> - i.e. APIVersions, KubeVersion.Version
- .Capabilities.<Name> - different Kubernetes clusters can have different capabilities and this is to check if they are available
- .Files.<Name> - a configuration file you want to pass to an application through a ConfigMap or Secret as a file in the chart
  - .Files.Get <name> - returns a content of a file as a string
  - .Files.GetBytes <name> - returns a content of a file as bytes,
  - .Files.Glob <pattern> - accepts a glob pattern and returns another files object containing only the files whose names match the pattern
  - .Files.AsConfig - takes a files group and returns it as flattened YAML suitable to include in the data section of a Kubernetes ConfigMap manifest. This is useful when paired with .Files.Glob. i.e. `{{ .Files.Glob("myconfig/**").AsConfig }} `
  - .Files.AsSecret - similar to .Files.AsConfig. Instead of returning flattened YAML it returns the data in a format that can be included in the data section of a Kubernetes Secret manifest. It’s Base64 encoded. This is useful when paired with .Files.Glob. For example, `{{ .Files.Glob("mysecrets/**").AsSecrets }}`.
  - .Files.Lines <file-name> - returns the contents of the file as an array split by newlines (i.e., \n).
    ```javascript
    apiVersion: v1
    kind: Secret
    metadata:
      name: {{ include "example.fullname" . }}
    type: Opaque
    data:
    {{ (.Files.Glob "config/*").AsSecrets | indent 2 }}
    ```
- .Template.<Name> - details about the current template being executed i.e. Name, BasePath

## Helm pipeline

```javascript
character: {{ .Values.character | default "Sylvester" | quote }}
```

There are three parts to this pipeline, each separated by a |. The first is .Values.character, which is a calculated value of character. This is either the value of character from the values.yaml file or one passed in when the chart is being rendered by helm install, helm upgrade, or helm template. This value is passed as the last argument to the default function. If the value is empty, default will use the value of “Sylvester” in its place. The output of default is passed as an input to quote, which ensures the value is wrapped in quotation marks. The output of quote is returned from the action.

## Template functions

- [Sprig Library](https://github.com/Masterminds/sprig)

```javascript
  securityContext:
    {{- toYaml .Values.podSecurityContext | nindent 8 }}
```

Popular functions:

- toYaml - converts object to YAML
- toJson - converts object to Json (more often used when creating configuration files to be passed to applications through Secrets and ConfigMaps)
- indent - indents each line
- nindent - inserts new line and indents each line

[Full list of template function](https://helm.sh/docs/chart_template_guide/function_list/)

## Querying Kubernetes Resources In Charts

The following example looks up a Deployment named "my-deployment-name" in the "my-namespace" namespace and makes the metadata annotations available:

```javascript
{{ (lookup "apps/v1" "Deployment" "my-namespace" "my-deployment-name").metadata.annotations }}
```

## Conditional Statements

- conditinal statement **if**/**else**/**with**

```javascript
{{- if .Values.ingress.enabled -}}
...
{{- else -}}
...
{{- end }}
```

- logical operators **and**, **or**, **eq**, **not**

```javascript
{{- if or (eq .Values.character "Wile E. Coyote") .Values.products -}}
...
{{- end }}
```

- conditional statement **with** which is similar to if with the caveat that the scope within a with block changes

```javascript
{{- with .Values.ingress.annotations }}
annotations:
  {{- toYaml . | nindent 4 }}
{{- end }}
```

If the value passed into with is empty, the block is skipped. If the value is not empty, the block is executed and the value of . inside the block is .Values.ingress.annotations.

## Variables

- when assigning a value to a new variable use **:=**

```javascript
{{ $var := .Values.character }}
character: {{ $var | default "Sylvester" | quote }}
```

- when assigning a new value to the existing variable use **=**

```javascript
{{ $var := .Values.character }}
{{ $var = "Tweety" }}
```

## Data Structures

- An example list in YAML

```javascript
animals: -Cow - Duck - Goose - Dog;
```

- An example map in YAML

```javascript
classes: cow: mammal;
duck: bird;
salmon: fish;
```

Within Helm templates you can create your own dictionaries and lists using the dict and list functions.

## Loops

- List - **range** iterates over each item in the list and sets the value of . to the value of each item in the list as Helm iterates over the item.

```javascript
animals:
{{- range .Values.animals }}
  - {{ . | quote }}
{{- end }}
```

- Map - **range** iterates over each item in the map and sets the value of . to the value of each item in the list as Helm iterates over the item.

```javascript
classes:
{{- range $key, $value := .Values.classes }}
  - {{ $key }}: {{ $value | quote }}
{{- end }}
```

## Templates

- Definition  
  A template is defined with a **define** statement followed by the name for the template.The content of a template is just like the content of any other template.The definition for a template is closed through an end statement that matches to the define statement.

```javascript
{{/*Selector labels */}}
{{- define "app-ngix.selectorLabels" -}}
  app.kubernetes.io/name: {{ include "app-ngix.name" . }}
  app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
```

- Usage  
  There are two functions you can use to include another template in your template. The template function is a basic function for including another template. It cannot be used in pipelines. Then there is the include function that works in a similar manner but can be used in pipelines.

```javascript
spec:
  replicas: {{ .Values.replicaCount }}
    selector:
      matchLabels:
        {{- include "app-ngix.selectorLabels" . | nindent 6 }}
    template:
      metadata:
        labels:
          {{- include "app-ngix.selectorLabels" . | nindent 8 }}
```

## Dependencies

Dependencies are specified in the Chart.yaml file. The following is an example of dependencies section in the Chart.yaml file...

```javascript
dependencies:
  - name: booster 1
    version: ^1.0.0 2
    condition: booster.enabled
    repository: https://raw.githubusercontent.com/Masterminds/learning-helm/main/
      chapter6/repository/
```

The repository field is where you specify the chart repository location to pull the dependency from. You can specify this in one of the following two ways:

- A URL to the Helm repository.
- To the name of a repository you have set up using the helm repo add command. This name needs to be preceded by an @ and wrapped in quotes (e.g., "@myrepo").

# Image pull secret

- [Create Image pull secrets](https://helm.sh/docs/howto/charts_tips_and_tricks/#creating-image-pull-secrets)
